////////////
//
// File:      SocketConnection.h
// Authors:   Mike Farnsworth
// Copyright: (C) 2016 Michael Farnsworth, All Rights Reserved.  See LICENSE for more info.
// Content:   Remote procedure call networked socket connection.
//
////////////

#ifndef __MetaRPC_SocketConnection_h__
#define __MetaRPC_SocketConnection_h__

#include <cstddef>
#include <string>

#include <MetaRPC/Connection.h>


namespace MetaRPC
{


///
/// \brief Default IP port for listener/server to use
///
const int kDefaultPort = 20157;


///
/// \brief Socket connection, for TCP/IP v4 and v6, as well as UNIX domain sockets
///
/// This connection class is capable of handling both local connections from one
/// process to another, as well as remote machines on the network.  The socket
/// can be in either listener
///
class SocketConnection : public Connection
{
public:
    ///
    /// \brief The type of socket to use for the connection
    ///
    enum SocketFlavor
    {
        kUnixDomainSocket,  ///< UNIX domain sockets, used for processes on the same machine
        kIPv4Socket,        ///< TCP/IPv4, can be used for local and remote machines
        kIPv6Socket         ///< TCP/IPv6, can be used for local and remote machines
    };

    ///
    /// \brief Construct a socket connection
    ///
    /// Construct a socket connection to communicate with another process/machine.
    ///
    /// \param connectDest           Destination of the socket; for UNIX domain
    ///                              sockets this is a file such as "/tmp/mrpc-asdf1234"
    ///                              which both listener and client use, while
    ///                              for IP connections from the client it is
    ///                              the hostname or IP address of the listener,
    ///                              and finally for IP listeners it is the name
    ///                              of the process' machine or just "localhost"
    /// \param listenForConnections  Whether this end of the connection is in
    ///                              listener mode or client mode
    /// \param socketFlavor          Socket type, UNIX domain sockets or IP variant
    /// \param connectPort           For IP sockets, the port to listen on or
    ///                              connect to (unused on UNIX domain sockets)
    /// \param noWait                For IP sockets, whether to write and flush
    ///                              data immediately upon write or instead wait
    ///                              for some to accumulate before it gets sent
    ///
    SocketConnection(const std::string& connectDest,
                     bool listenForConnections = false,
                     SocketFlavor socketFlavor = kIPv4Socket,
                     int connectPort = kDefaultPort,
                     bool noWait = true);

    virtual ~SocketConnection();

    virtual void update();

    virtual bool write(const void *data, size_t numBytes);
    virtual bool read(void *data, size_t numBytes);
    virtual bool peek(void *data, size_t numBytes);

    virtual void flush() { /* TODO: for m_noDelay == false we should actually do something here */ }

    virtual size_t numBytesAvailable();

    virtual void close();

    ///
    /// \brief Whether we are in listening mode (server) or not
    ///
    bool listener() const { return m_listenMode; }

    ///
    /// \brief Whether we are in client mode (non-server) or not
    ///
    bool client()   const { return !m_listenMode; }

    ///
    /// \brief For IP connections, the port used for the connection
    /// \return port number
    ///
    int port() const { return m_port; }

    ///
    /// \brief Remote destination, e.g. host for IP and file for UNIX domain sockets
    /// \return remote destination
    ///
    const std::string& remote() const { return m_remote; }

    ///
    /// \brief The type of socket used for connections, UNIX domain socket or IPv4 or IPv6
    /// \return type of socket
    ///
    SocketFlavor flavor() const { return m_flavor; }

protected:
    int	m_socketFD, m_clientSocketFD;
    int m_port;
    bool m_noDelay;
    bool m_listenMode;
    std::string m_remote;
    SocketFlavor m_flavor;
};


} // namespace MetaRPC


#endif // __MetaRPC_SocketConnection_h__
