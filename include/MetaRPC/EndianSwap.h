////////////
//
// File:      EndianSwap.h
// Authors:   Mike Farnsworth
// Copyright: (C) 2016 Michael Farnsworth, All Rights Reserved.  See LICENSE for more info.
// Content:   Endianness swapping utilities for RPC network communication
//
////////////

#ifndef __MetaRPC_EndianSwap_h__
#define __MetaRPC_EndianSwap_h__

#include <cstdint>


namespace MetaRPC
{


///
/// \brief Swap a type to the opposite endianness
///
/// The generic version of this does nothing, and assumes no swapping is needed.
/// You should specialize this template for your type if it needs swapping.
///
template <typename T>
T endianSwap(T value)
{
    return value;
}


#if !defined(METARPC_FORCE_SAME_ENDIANNESS) || METARPC_FORCE_SAME_ENDIANNESS == 0


//
// Built-in endianness conversion routines
//

template<>
inline int16_t endianSwap(int16_t value)
{
    union { int16_t v; struct { int8_t swap[2]; }; } pre, post;
    pre.v = value;
    post.swap[0] = pre.swap[1];
    post.swap[1] = pre.swap[0];
    return post.v;
}

template<>
inline int32_t endianSwap(int32_t value)
{
    union { int32_t v; struct { int8_t swap[4]; }; } pre, post;
    pre.v = value;
    post.swap[0] = pre.swap[3];
    post.swap[1] = pre.swap[2];
    post.swap[2] = pre.swap[1];
    post.swap[3] = pre.swap[0];
    return post.v;
}

template<>
inline int64_t endianSwap(int64_t value)
{
    union { int64_t v; struct { int8_t swap[8]; }; } pre, post;
    pre.v = value;
    post.swap[0] = pre.swap[7];
    post.swap[1] = pre.swap[6];
    post.swap[2] = pre.swap[5];
    post.swap[3] = pre.swap[4];
    post.swap[4] = pre.swap[3];
    post.swap[5] = pre.swap[2];
    post.swap[6] = pre.swap[1];
    post.swap[7] = pre.swap[0];
    return post.v;
}

template<>
inline float endianSwap(float value)
{
    union { float v; struct { int8_t swap[4]; }; } pre, post;
    pre.v = value;
    post.swap[0] = pre.swap[3];
    post.swap[1] = pre.swap[2];
    post.swap[2] = pre.swap[1];
    post.swap[3] = pre.swap[0];
    return post.v;
}

template<>
inline double endianSwap(double value)
{
    union { double v; struct { int8_t swap[8]; }; } pre, post;
    pre.v = value;
    post.swap[0] = pre.swap[7];
    post.swap[1] = pre.swap[6];
    post.swap[2] = pre.swap[5];
    post.swap[3] = pre.swap[4];
    post.swap[4] = pre.swap[3];
    post.swap[5] = pre.swap[2];
    post.swap[6] = pre.swap[1];
    post.swap[7] = pre.swap[0];
    return post.v;
}


#endif // !defined(METARPC_FORCE_SAME_ENDIANNESS) || METARPC_FORCE_SAME_ENDIANNESS == 0


///
/// \brief Conditional endian swap (convenience function)
///
template<typename T>
void endianSwap(T& value, bool doSwap)
{
    if (doSwap)
    {
        value = endianSwap<T>(value);
    }
}


} // namespace MetaRPC


#endif // __MetaRPC_EndianSwap_h__
