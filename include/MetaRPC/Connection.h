////////////
//
// File:      Connection.h
// Authors:   Mike Farnsworth
// Copyright: (C) 2016 Michael Farnsworth, All Rights Reserved.  See LICENSE for more info.
// Content:   Base class for connection transports underlying RPC streams.
//
////////////

#ifndef __MetaRPC_Connection_h__
#define __MetaRPC_Connection_h__

#include <cstddef>


namespace MetaRPC
{


///
/// \brief Base class for all RPC connections
///
/// Base class for all transport mechanisms underlying an RPC stream.  To create
/// one, you must implement all pure virtual methods, and when the stream is
/// actually open make sure to set m_isOpen to true.  RPCStream takes care of
/// the rest.  There are two pre-built subclasses for handling same-process
/// (RPCMemConnection), interprocess (RPCSocketConnection in UNIX domain socket
/// mode), and network communication (RPCSocketConnection in IPv4 or IPv6 mode).
///
class Connection
{
public:
    Connection() : m_isOpen(false) { }

    virtual ~Connection() { }

    virtual bool   write(const void *buffer, size_t numBytes) = 0;
    virtual bool   read(       void *buffer, size_t numBytes) = 0;
    virtual bool   peek(       void *buffer, size_t numBytes) = 0;

    ///
    /// \brief Retrieve amount of data already received on the connection
    ///
    /// Check for the number of bytes available on the connection to be read or
    /// peeked.  If there is a disconnection, calling this will notice that has
    /// happened and close the connection.
    ///
    /// \return amount of data in bytes
    ///
    virtual size_t numBytesAvailable() = 0;

    ///
    /// \brief Close the connection
    ///
    virtual void close() = 0;

    ///
    /// \brief Update the state of the connection
    ///
    /// Updating the connection will find if a remote party has connected, and
    /// otherwise check the health of the connection.  Any periodic work that
    /// needs to be done by the connection happens here.
    ///
    virtual void update() = 0;

    ///
    /// \brief Flush any data waiting to be written to the connection
    ///
    virtual void flush() = 0;

    ///
    /// \brief Current connected stated of the connection
    ///
    /// The connection state can be made current by calling either
    /// \ref update(), \ref numBytesAvailable(), or any of the read/write
    /// methods.  After that, this method should be accurate.
    ///
    /// \return whether the connection is currently connected
    ///
    virtual bool isOpen() const { return m_isOpen; }

protected:
    bool m_isOpen;
};


} // namespace MetaRPC


#endif // __MetaRPC_Connection_h__
