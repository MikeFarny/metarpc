////////////
//
// File:      Exception.h
// Authors:   Mike Farnsworth
// Copyright: (C) 2016 Michael Farnsworth, All Rights Reserved.  See LICENSE for more info.
// Content:   Remote procedure call exceptions
//
////////////

#ifndef __MetaRPC_Exception_h__
#define __MetaRPC_Exception_h__

#include <stdexcept>
#include <string>


namespace MetaRPC
{


// Forward declarations
class Stream;
class Connection;
class ProcedureBase;


///
/// \brief Common base class for all RPC exceptions (if you want to catch all types)
///
class Exception : public std::runtime_error
{
public:
    Exception(const std::string& desc) : std::runtime_error(desc) { }
};


///
/// \brief Exceptions that occur for connections
///
class ConnectionException : public Exception
{
public:
    ConnectionException(const Connection* pConnection, const std::string& desc)
        : Exception(desc), m_pConnection(pConnection) { }

    const Connection* connection() const { return m_pConnection; }

private:
    const Connection* m_pConnection;
};


///
/// \brief Exceptions that occur when attempting to call remote procedures
///
class RemoteProcedureCallException : public Exception
{
public:
    RemoteProcedureCallException(const std::string& procedureName, const std::string& desc)
        : Exception(desc), m_procedureName(procedureName) { }

    const std::string& procedureName() const { return m_procedureName; }

private:
    std::string m_procedureName;
};


///
/// \brief Exceptions that occur when a remote side calls local functions
///
class LocalProcedureCallException : public Exception
{
public:
    LocalProcedureCallException(const ProcedureBase* pLocalProcedure, const std::string& desc)
        : Exception(desc), m_pLocalProcedure(pLocalProcedure) { }

    const ProcedureBase* localProcedure() const { return m_pLocalProcedure; }

private:
    const ProcedureBase* m_pLocalProcedure;
};


///
/// \brief Exceptions that occur during encoding (calling remote) or decoding
///        (calling local or getting return values)
///
class EncodeDecodeException : public Exception
{
public:
    EncodeDecodeException(const std::string& desc) : Exception(desc) { }
};


///
/// \brief Exceptions that occur when interpreting RPC streams, both for local
///        and remote calls
///
class StreamException : public Exception
{
public:
    StreamException(const Stream* pStream, const std::string& desc)
        : Exception(desc), m_pStream(pStream) { }

    const Stream* stream() const { return m_pStream; }

private:
    const Stream *m_pStream;
};


} // namespace MetaRPC


#endif // __MetaRPC_Exception_h__
