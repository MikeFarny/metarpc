////////////
//
// File:      RPCStream.h
// Authors:   Mike Farnsworth
// Copyright: (C) 2016 Michael Farnsworth, All Rights Reserved.  See LICENSE for more info.
// Content:   Remote procedure call managers.
//
////////////

#ifndef __MetaRPC_Stream_h__
#define __MetaRPC_Stream_h__

#include <cstddef>
#include <cstdint>
#include <map>
#include <vector>
#include <string>

#include <MetaRPC/DataChunk.h>
#include <MetaRPC/Connection.h>


namespace MetaRPC
{


// Forward declaration
class ProcedureBase;


///
/// \brief The main RPC stream interface
///
/// The RPC stream ties local procedures, remote procedures, and the underlying
/// connection all together.  Create the connection that the stream will be
/// relying on first, and then create the stream over that connection.  After
/// that you will be ready to register local procedures that the remote end can
/// call (with \ref Stream::registerProcedure()).  Remote procedures are
/// created with a reference to the stream, and when called will automatically
/// go out over this stream.
///
/// You must call \ref Stream::update() periodically in the same thread that
/// you call remote procedures.  This allows local procedures to be run by the
/// remote caller.
///
/// \note The stream must not go out of scope or be destructed before remote
/// procedures are called.
///
/// \note This is not thread safe.  Currently, all local procedures, remote
/// procedure calls, and \ref Stream::update() must be done in the same thread.
///
class Stream
{
    typedef std::multimap<std::string, ProcedureBase *> LocalProceduresMap;
    typedef std::pair<std::string, ProcedureBase *> LocalProceduresMapEntry;
    typedef std::pair<LocalProceduresMap::iterator,
                      LocalProceduresMap::iterator> LocalProceduresMapRange;

public:
    ///
    /// \brief Construct an RPC stream over a connection
    ///
    /// As soon as you construct, the connection will be used!
    /// Make sure the underlying connection is ready before you
    /// create a RPCStream!
    ///
    Stream(Connection& underlyingConnection);

    ~Stream()
    {
        close();
    }

    ///
    /// \brief Register a local procedure so that it is callable by the remote side
    ///
    /// \param procedure  The local procedure to register
    ///
    void registerProcedure(ProcedureBase& procedure);

    ///
    /// \brief Unregister a local procedure so that it is not callable by the remote side
    ///
    /// \param procedure  The local procedure to unregister
    /// \return whether the procedure was found and unregistered
    ///
    bool unregisterProcedure(ProcedureBase& procedure);

    ///
    /// \brief Unregister all local procedures so that none are callable by the remote side
    ///
    void unregisterAllProcedures();

    ///
    /// \brief Call a remote procedure
    ///
    /// You should not call this directly unless you know what you are doing.
    /// Instead, you should create a \ref RemoteProcedure using function-style
    /// syntax on the stream, and then call it (which will invoke it here).
    ///
    /// \note This will block until a return value comes in (unless the function
    /// has a void return), and if any pending local procedures are ahead in the
    /// data queue those will be invoked during this call (e.g. calling a remote
    /// function with a non-void return will call \ref update() internally!)
    ///
    bool invokeRPC(const std::string& procedureName,
                   DataChunk& returnChunk,
                   const uint32_t numArgs,
                   DataChunk argChunks[]);

    ///
    /// \brief Read incoming data, invoke local procedures as they come in
    ///
    /// \note You must call this periodically, in the same thread you call
    /// remote procedures and wish to have local procedures invoked.
    void update();

    ///
    /// \brief Close the current remote connection
    ///
    void close();

    ///
    /// \brief Get the connection to the remote end
    /// \return the remote connection
    ///
    const Connection& connection() const { return m_connection; }
    ///
    /// \brief Get the connection to the remote end
    /// \return the remote connection
    ///
    Connection&       connection()       { return m_connection; }

    ///
    /// \brief Check status of endian swapping
    ///
    /// Is the other end of the stream the opposite endianness from us?
    /// (Note this value is not accurate unless the connection has already been
    /// established.)  When true, this can impact performance negatively.
    ///
    bool doEndianSwap() const { return m_endianSwap; }

    ///
    /// \brief Clone all local procedures to another stream, so it can invoke them too
    ///
    /// \param destStream  The stream to clone all local procedures to
    ///
    void cloneRegisteredProcedures(Stream& destStream);

private:
    LocalProceduresMap m_localProcedures;
    Connection& m_connection;
    bool m_endianSwap;
    char *m_buffer;
    int32_t m_bufferFilled, m_bufferSize;
    int32_t m_uniqueCallId;

    enum ReadType
    {
        kReadingNone,
        kReadingProcedureCall,
        kReadingReturnValue,
        kReturnValueWaiting
    };
    ReadType m_readStatus;
    
    enum StreamReadiness
    {
        kNotReady,
        kSentHandshake,
        kReceivedHandshake
    };
    StreamReadiness m_readiness;

    void invokeLocalProcedure(const int32_t procCallId,
                              const std::string& procedureName,
                              const uint32_t numArgs,
                              DataChunk argChunks[]);

    void sendReturnChunk(const int32_t procCallId,
                         const std::string& procedureName,
                         DataChunk& returnChunk);
    bool writeChunk(DataChunk& chunk);

    void hardClose();
};


/*
 * RPC protocol:
 *
 * On initialization, send cookie 0xCAFEBEEF, without any endianess
 * conversion (identifies endianness as well that way, as well as the
 * protocol).  The receiving end of the data will endian-swap if the
 * endianness of the two connection end points do not match, so the
 * host should always send in its native endian layout.
 *
 * On RPC call, send cookie 0xBEEFF00D,
 *		total procedure call size in bytes (4 bytes, totals everything
 * 			that follows in the procedure, excluding cookie + total size header),
 * 		procedure call ID (4 bytes; uniquely identifies return value later),
 * 		procedure name length in bytes (4 bytes),
 * 		procedure name (n bytes),
 * 		num params (4 bytes), then a chunk for each parameter.
 * 		Finally, wait for return value if the procedure expects one.
 * Return values are sent back as first a cookie 0xCAFEF00D,
 * 		then the procedure call ID (4 bytes),
 * 		then a single chunk.
 * Chunk format is size in bytes (4 bytes),
 * 		type ID (4 bytes),
 * 		and then data (n bytes).
 *
 * On a (nice) connection close, the closing end sends cookie 0xDEADF00D.
 * No reply from the remote end is necessary.
 *
 * NOTES: The type IDs for each parameter must be agreed-upon before
 * connection.  Also, a procedure currently must be uniquely identified
 * by name + num params + param types must match; this allows
 * overloading of procedures by name, where they only have to
 * differ in parameters to be unique.  Differing only by return type
 * is not sufficient.
 *
 * Finally, MetaRPC is currently not thread-safe; e.g. if you create an
 * Stream, you must only call remote procedures for that stream in
 * the same thread that you call Stream::update() for that stream;
 * this also means that any local procedures called on that stream
 * happen in the same thread.
 *
 */


} // namespace MetaRPC


#endif // __MetaRPC_Stream_h__
