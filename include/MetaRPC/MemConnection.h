////////////
//
// File:      MemConnection.h
// Authors:   Mike Farnsworth
// Copyright: (C) 2016 Michael Farnsworth, All Rights Reserved.  See LICENSE for more info.
// Content:   Remote procedure call shared-memory connection.
//
////////////

#ifndef __MetaRPC_MemConnection_h__
#define __MetaRPC_MemConnection_h__

#include <boost/thread.hpp>

#include <MetaRPC/Connection.h>


namespace MetaRPC
{


///
/// \brief Default size of a memory connection, 1 MB
///
const size_t kMemConnectionDefaultSize = 1024 * 1024; // 1 MB


///
/// \brief Shared memory connection for use within the same process
///
/// For both ends of a connection an instance of this class is required.  They
/// are connected together via \ref MemConnection::setRemote() on both connections.
///
/// \note This connection type is thread-safe
///
class MemConnection : public Connection
{
public:
    ///
    /// \brief Construct a new memory connection
    /// \param bufferSize  The memory buffer size to use, which should be large
    ///                    enough to accomodate all the data in your largest
    ///                    remote procedure call
    ///
    MemConnection(size_t bufferSize = kMemConnectionDefaultSize);

    virtual ~MemConnection();

    ///
    /// \brief Set the remote memory connection to communicate with
    /// \param connection  The remote memory connection to pair with
    ///
    void setRemote(MemConnection& connection) { m_remote = &connection; }

    virtual bool write(const void *data, size_t numBytes);
    virtual bool read(void *data, size_t numBytes);
    virtual bool peek(void *data, size_t numBytes);

    virtual void flush() { /* Nothing needed */ }

    virtual void update() { /* Nothing needed */ }

    virtual size_t numBytesAvailable()
    {
        size_t numAvailable;
        {
            boost::mutex::scoped_lock lock(m_rwMutex);
            if (m_readIndex <= m_writeIndex)
            {
                numAvailable = m_writeIndex - m_readIndex;
            }
            else
            {
                numAvailable = m_size - m_readIndex + m_writeIndex;
            }
        }
        return numAvailable;
    }

    virtual void close() { m_isOpen = false; }

private:
    MemConnection *m_remote;
    unsigned char *m_buffer;
    size_t m_writeIndex, m_readIndex, m_size;
    boost::mutex m_rwMutex;
};


} // namespace MetaRPC


#endif // __MetaRPC_MemConnection_h__
