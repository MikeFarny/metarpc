////////////
//
// File:      ReturnSupport.h
// Authors:   Mike Farnsworth
// Copyright: (C) 2016 Michael Farnsworth, All Rights Reserved.  See LICENSE for more info.
// Content:   Regular/void return support for RPC.
//
////////////

#ifndef __MetaRPC_ReturnSupport_h__
#define __MetaRPC_ReturnSupport_h__

#include <boost/signals2/signal.hpp>

#include <MetaRPC/DataChunk.h>


namespace MetaRPC
{
    namespace Detail
    {


//
// Handle decoding return values, specialize for 'void'
//

template<typename ReturnType>
ReturnType decodeReturnWithVoidSupport(DataChunk& returnChunk, bool endianSwap)
{
    ReturnType returnValue;
    decode<ReturnType>(returnValue, returnChunk, endianSwap);
    return returnValue;
}

template<>
inline void decodeReturnWithVoidSupport<void>(DataChunk& data, bool endianSwap)
{
    // Eat it, sucker!
}


//
// Raise locally-connected signals, specialize to handle void returns
//

template<typename Signature>
class SignalRaiser;

template<typename ReturnType, typename... Args>
class SignalRaiser< ReturnType (Args...) >
{
public:
    typedef boost::signals2::signal< ReturnType (Args...) > SignalType;

    void raise(boost::signals2::signal< ReturnType (Args...) >& signal,
               DataChunk& returnChunk,
               Args... args)
    {
        ReturnType returnValue = *signal(args...);
        encode<ReturnType>(returnValue, returnChunk);
    }
};

// Specialize for void return (just signal and go)
template<typename... Args>
class SignalRaiser< void (Args...) >
{
public:
    typedef boost::signals2::signal< void (Args...) > SignalType;

    void raise(boost::signals2::signal< void (Args...) >& signal,
               DataChunk&,
               Args... args)
    {
        signal(args...);
    }
};


    } // namespace Detail
} // namespace MetaRPC


#endif // __MetaRPC_ReturnSupport_h__
