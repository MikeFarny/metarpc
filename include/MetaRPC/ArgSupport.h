////////////
//
// File:      ArgSupport.h
// Authors:   Mike Farnsworth
// Copyright: (C) 2016 Michael Farnsworth, All Rights Reserved.  See LICENSE for more info.
// Content:   Argument encode/decode/enumeration support for RPC procedures
//
////////////

#ifndef __MetaRPC_ArgSupport_h__
#define __MetaRPC_ArgSupport_h__

#include <cstdint>
#include <string>
#include <boost/signals2/signal.hpp>

#include <MetaRPC/EndianSwap.h>
#include <MetaRPC/DataChunk.h>
#include <MetaRPC/ReturnSupport.h>
#include <MetaRPC/Stream.h>


namespace MetaRPC
{
    namespace Detail
    {


//
// Helpers for remote procedures
//

inline void encodeArguments(DataChunk*)
{
    // This points one past the array of chunks, so nothing to encode
}

template<typename ArgType, typename... Args>
void encodeArguments(DataChunk *argChunks, ArgType& arg, Args... args)
{
    encode<ArgType>(arg, *argChunks);
    encodeArguments(argChunks + 1, args...);
}


//
// Helpers for local procedures
//

template<uint32_t index>
void buildArgTypes(std::vector<DataType>&)
{
    // Do nuthin'
}


template<uint32_t index, typename ArgType, typename... Args>
void buildArgTypes(std::vector<DataType>& outTypes)
{
    outTypes.push_back(static_cast<DataType>(EncodingType<ArgType>::TypeDesc));
    buildArgTypes<index + 1, Args...>(outTypes);
}


// Helper for void argument/return type decoding (e.g. do nothing)
template<uint32_t index>
void decodeArgument(const DataChunk*, bool)
{

}

// Helper for decoding an argument by a fixed index
template<uint32_t index, typename ArgType>
ArgType decodeArgument(const DataChunk *argChunks, bool endianSwap)
{
    ArgType arg;
    decode<ArgType>(arg, argChunks[index], endianSwap);
    return arg;
}

// Helper for creating compile-time argument indices
template<uint32_t index, typename... Args>
struct ArgumentEnumerator
{
    enum { kIndex = ArgumentEnumerator<index - 1, Args...>::kIndex + 1 };
};

template<typename... Args>
struct ArgumentEnumerator<0, Args...>
{
    enum { kIndex = 0 };
};


    } // namespace Detail
} // namespace MetaRPC


#endif // __MetaRPC_ArgSupport_h__
