////////////
//
// File:      DataChunk.h
// Authors:   Mike Farnsworth
// Copyright: (C) 2016 Michael Farnsworth, All Rights Reserved.  See LICENSE for more info.
// Content:   Generic data packaging for RPC
//
////////////

#ifndef __MetaRPC_DataChunk_h__
#define __MetaRPC_DataChunk_h__

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <string>
#include <sstream>

#include <MetaRPC/EndianSwap.h>
#include <MetaRPC/EncodingTypes.h>
#include <MetaRPC/Exceptions.h>


namespace MetaRPC
{


///
/// \brief Generic data encapsulated and ready for network transfer
///
/// Chunks have data (to be transported across the RPC connection), a size for
/// that data, and a type that uniquely idenfies the layout of the data contents.
/// Each argument and return value in an RPC call gets wrapped up in a chunk.
///
class DataChunk
{
public:
    DataChunk() : m_size(0), m_type(kDataTypeUnknown), m_data(nullptr) { }

    explicit DataChunk(int32_t size, const DataType& type, void *data)
        : m_size(size), m_type(type), m_data(reinterpret_cast<unsigned char *>(data))
    {
        m_data = new unsigned char[m_size];
        std::memcpy(m_data, data, m_size);
    }

    ~DataChunk() { delete[] m_data; }

    int32_t  size() const          { return m_size; }
    int32_t& size()                { return m_size; }
    void     setSize(int32_t size) { m_size = size; }

    DataType  type() const                  { return m_type; }
    DataType& type()                        { return m_type; }
    void      setType(const DataType& type) { m_type = type; }

    const void* data() const { return static_cast<void *>(m_data); }
    void*       data()       { return static_cast<void *>(m_data); }

    void setData(const void *data)
    {
        if (m_data != 0)
        {
            delete[] m_data;
        }
        m_data = new unsigned char[m_size];
        std::memcpy(m_data, data, m_size);
    }

private:
    // Forbidden
    DataChunk(const DataChunk&)             = delete;
    DataChunk& operator =(const DataChunk&) = delete;

    int32_t m_size;
    DataType m_type;
    unsigned char *m_data;
};


//
// Encode/decode methods for custom data
//

///
/// \brief Chunk encoder
///
/// You really should specialize these; without doing so your code may run, but
/// will not be able to handle endianess, etc.  Also, if you fail to specify a
/// type for your encode, this is considered a failure!
///
template<typename T>
void encode(T& data, DataChunk& output)
{
    output.setSize(sizeof(T));
    output.setType(static_cast<DataType>(EncodingType<T>::TypeDesc));
    output.setData(&data);
    if (static_cast<DataType>(EncodingType<T>::TypeDesc) == kDataTypeUnknown)
    {
        std::ostringstream errorStream;
        errorStream << "Unknown type encountered while encoding data, size " << sizeof(T);
        throw EncodeDecodeException(errorStream.str());
    }
}


///
/// \brief Chunk decoder
///
/// You really should specialize this for the types you want to be able to
/// serialize and deserialize; this does not (and cannot!) fully check the type
/// of what is sent across the network to match up with type T!
///
template<typename T>
void decode(T& output, const DataChunk& input, bool doEndianSwap)
{
    // Sanity checks
    if (static_cast<DataType>(EncodingType<T>::TypeDesc) == kDataTypeUnknown)
    {
        std::ostringstream errorStream;
        errorStream << "Unknown data chunk type while decoding, size " << sizeof(T);
        throw EncodeDecodeException(errorStream.str());
    }
    else if (input.type() != static_cast<DataType>(EncodingType<T>::TypeDesc))
    {
        std::ostringstream errorStream;
        errorStream << "Type mismatch decoding data, expected type "
                    << static_cast<DataType>(EncodingType<T>::TypeDesc)
                    << " but got " << input.type();
        throw EncodeDecodeException(errorStream.str());
    }
    else if (sizeof(T) != input.size())
    {
        std::ostringstream errorStream;
        errorStream << "Size mismatch decoding data, expected size " << sizeof(T)
                    << " but got " << input.size();
        throw EncodeDecodeException(errorStream.str());
    }

    // Success!  Record the generic data, attempt endianness conversion
    if (doEndianSwap)
    {
        output = endianSwap<T>(*reinterpret_cast<const T *>(input.data()));
    }
    else
    {
        output = *reinterpret_cast<const T *>(input.data());
    }
}


//
// Built-in specialized encode() and decode() implementations for complex types
// (so far we only do this for std::string)
//


template<>
inline void encode<std::string>(std::string& data, DataChunk& output)
{
    // NOTE: we are leaving off the null terminator!
    output.setSize(data.length());
    output.setType(static_cast<DataType>(EncodingType<std::string>::TypeDesc));
    output.setData(data.c_str());
}


template<>
inline void decode<std::string>(std::string& output, const DataChunk& input, bool doEndianSwap)
{
    if (input.type() != static_cast<DataType>(EncodingType<std::string>::TypeDesc))
    {
        std::ostringstream errorStream;
        errorStream << "Type mismatch decoding data, expected type "
                    << static_cast<DataType>(EncodingType<std::string>::TypeDesc)
                    << " but got " << input.type();
        throw EncodeDecodeException(errorStream.str());
    }
    output = std::string(reinterpret_cast<const char *>(input.data()), input.size());
}


} // namespace MetaRPC


#endif // __MetaRPC_DataChunk_h__
