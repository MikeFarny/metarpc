////////////
//
// File:      Procedure.h
// Authors:   Mike Farnsworth
// Copyright: (C) 2016 Michael Farnsworth, All Rights Reserved.  See LICENSE for more info.
// Content:   Remote/local procedure creation.
//
////////////

#ifndef __MetaRPC_Procedure_h__
#define __MetaRPC_Procedure_h__

#include <string>
#include <sstream>
#include <boost/signals2/signal.hpp>

#include <MetaRPC/EndianSwap.h>
#include <MetaRPC/DataChunk.h>
#include <MetaRPC/ReturnSupport.h>
#include <MetaRPC/ArgSupport.h>
#include <MetaRPC/Stream.h>
#include <MetaRPC/Exceptions.h>


namespace MetaRPC
{


///
/// \brief Remote procedure
///
template<typename Signature>
class RemoteProcedure;

///
/// \brief Remote procedure with function signature
///
template<typename ReturnType, typename... Args>
class RemoteProcedure< ReturnType (Args...) >
{
public:
    typedef ReturnType ResultType;

    RemoteProcedure(Stream& stream, const std::string& procName)
        : m_stream(stream), m_procedureName(procName) { }
    
    virtual ~RemoteProcedure() { }

    virtual uint32_t numArguments() const { return static_cast<uint32_t>(sizeof...(Args)); }

    virtual DataType returnType() const
    {
        return static_cast<DataType>(EncodingType<ReturnType>::TypeDesc);
    }

    ReturnType operator()(Args... args)
    {
        // TODO: catch decode exceptions and turn them into remote procedure call exceptions
        DataChunk argChunks[sizeof...(args)];
        try
        {
            Detail::encodeArguments(argChunks, args...);
            DataChunk returnChunk;
            returnChunk.setType(static_cast<DataType>(EncodingType<ReturnType>::TypeDesc));
            if (!m_stream.invokeRPC(m_procedureName, returnChunk, sizeof...(args), argChunks))
            {
                throw RemoteProcedureCallException(m_procedureName, "Failed to invoke remote procedure");
            }
            return Detail::decodeReturnWithVoidSupport<ReturnType>(returnChunk, m_stream.doEndianSwap());
        }
        catch (EncodeDecodeException& ede)
        {
            throw RemoteProcedureCallException(m_procedureName,
                                               std::string("Failed to invoke remote procedure: ") + ede.what());
        }
    }

protected:
    Stream& m_stream;
    std::string m_procedureName;
};


/* Example usage of remote procedures:

typedef RemoteProcedure< int (int, int) > DoWhateverProc;
DoWhateverProc doWhatever(rpcStream, "DoWhatever");
int result = doWhatever(2, 3);

// You can leave out the typedef, to get:
RemoteProcedure< int (int, int) > doWhateverAlt(rpcStream, "DoWhatever");
result = doWhateverAlt(2, 3);

// Or for the insane:
result = RemoteProcedure< int (int, int) >(rpcStream, "DoWhatever")(2, 3);

*/


//
// Local procedure
//

///
/// \brief Local procedures (callable from elsewhere) base class
///
/// Base class for all locally-implemented procedures.  You really should just
/// use the templated class Procedure instead, though (see below).
///
class ProcedureBase
{
public:
    ProcedureBase(const std::string& procName) : m_name(procName), m_argTypes() { }

    virtual ~ProcedureBase() { }

    virtual void operator ()(DataChunk& returnChunk,
                             uint32_t numParams,
                             const DataChunk paramChunks[],
                             bool endianSwap) = 0;

    virtual uint32_t numArguments() const = 0;
    virtual DataType returnType() const = 0;

    virtual bool equals(const ProcedureBase& proc)
    {
        // TODO: hash the name + arg types to speed this up?
        if (numArguments() != proc.numArguments() || m_name != proc.m_name)
        {
            return false;
        }
        for (uint32_t i = 0; i < numArguments(); ++i)
        {
            if (m_argTypes[i] != proc.m_argTypes[i])
            {
                return false;
            }
        }
        return true;
    }

    const std::string& name() const { return m_name; }
    std::string&       name()       { return m_name; }

protected:
    std::string m_name;
    std::vector<DataType> m_argTypes;
};


///
/// \brief Template for local procedures (callable from elsewhere)
///
template<typename Signature>
class Procedure;

///
/// \brief Template for function-style local procedures (callable from elsewhere)
///
template<typename ReturnType, typename... Args>
class Procedure< ReturnType (Args...) > : public ProcedureBase
{
public:
    typedef boost::signals2::signal< ReturnType (Args...) > SignalType;
    typedef ReturnType ResultType;


    Procedure(const std::string& procName)
        : ProcedureBase(procName), m_signal() { Detail::buildArgTypes<0, Args...>(m_argTypes); }


    virtual uint32_t numArguments() const { return static_cast<uint32_t>(sizeof...(Args)); }

    virtual DataType returnType() const
    {
        return static_cast<DataType>(EncodingType<ReturnType>::TypeDesc);
    }


    virtual void operator ()(DataChunk& returnChunk,
                             uint32_t numArgs,
                             const DataChunk argChunks[],
                             bool endianSwap)
    {
        // TODO: catch decode exceptions and turn them into local procedure call exceptions
        if (numArgs != sizeof...(Args))
        {
            std::ostringstream errorStream;
            errorStream << "Mismatched number of params, expected  " << sizeof...(Args)
                        << " but got " << numArgs;
            throw LocalProcedureCallException(this, errorStream.str());
        }

        Detail::SignalRaiser< ReturnType (Args...) > raiser;
        raiser.raise(m_signal,
                     returnChunk,
                     Detail::decodeArgument<Detail::ArgumentEnumerator<0, Args>::kIndex, Args>(argChunks, endianSwap)...);
    }

    SignalType&	signal() { return m_signal; }

protected:
    SignalType m_signal;
};


/* Example creation of local procedures:

typedef Procedure< int (int, int) > DoWhateverProc;
DoWhateverProc doWhatever("DoWhatever");

int DoWhateverImpl(int arg1, int arg2)
{
    return 0;
}

class X : public boost::signals2::trackable // Do this or else you must disconnect manually!
{
public:
    int DoWhateverImpl2(int arg1, int arg2)
    {
        return 1;
    }
};

class Y : public boost::signals2::trackable // Do this or else you must disconnect manually!
{
public:
    int operator ()(int arg1, int arg2)
    {
        return 2;
    }
};

...
doWhatever.signal().connect(&DoWhateverImpl);
doWhatever.signal().connect(boost::bind(X::DoWhateverImpl2, xInstance, _1, _2, _3));
doWhatever.signal().connect(yInstance);

someStream.registerProcedure(doWhatever);

...

// in a main loop somewhere:
someStream.update();

*/


} // namespace MetaRPC


#endif // __MetaRPC_Procedure_h__
