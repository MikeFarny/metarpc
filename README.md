# MetaRPC #

MetaRPC is a templated remote procedure call (RPC) library for C++.  Using compile-time constructs it aims to take some of the annoyance out of creating local and remote functions, methods and function objects that are callable like normal C++ functions.

MetaRPC is released under the new BSD License.  Please see LICENSE for more information.

### Capabilities ###

* Currently it supports Linux, Mac OS X, and probably other UNIX-like OSs.  Windows is not yet supported.
* Connect within the process (memory stream), on the same host (UNIX domain sockets), or across the network (IPv4 and IPv6)
* Register local and remote procedures by function-style syntax:
```
#!c++
Procedure<void (int, int)> localProc("myProc");
RemoteProcedure<int (int, int)> remoteProc(myRPCStream, "theirProc");
```
* Call remote procedures with normal syntax:
```
#!c++
int result = remoteProc(intArg1, intArg2);
```
* Multiple procedures of the same name can be registered, so long as they differ in arguments (function overloading).
* Multiple local functions, etc can be connected to one local procedure at the same time, and all will be called.
* User can add support for additional data types to be sent over the wire, by creating simple encode/decode and possibly optional endian-swap specializations.

### Dependencies ###

MetaRPC relies on the following:

* Boost.Signals2
* Boost.Thread (and the platform threading library such as pthread)
* platform sockets library
* C++11 capable compiler

### Build from Source ###

Building is currently done via a very simple makefile.  Available targets are `all` and `clean`.  There are two tests/examples that are built: `localtest` and `conn_test`.

After making boost available, compilation is straightforward:

* `make BOOST_HOME=/path/to/boost/root all`

Defines that affect compilation:

* Defining `METARPC_FORCE_SAME_ENDIANESS` will cause all endian-swapping code to compile out.  If you know that all RPC communication will happen on machines with identical endianess, then defining this can produce a speed advantage.

Documentation:

* Run doxygen in the main directory, and it will output `doc/html/` containing API reference for using MetaRPC.

### Contributing ###

For contributing code or making pull requests:

* Follow the existing code style
* Test your submissions thoroughly, and using all three connection types if possible.  Bugs can be hard to catch with networking, so it's imperative to stress any proposed code changes well before they are committed.
* Contributions will be accepted or rejected based purely upon their merit.  Any suggestions otherwise will result in removal of the suggester from the project.

### Contact/Bugs ###

* If you have a bug report, feature request, or other similar change please [file an issue](https://bitbucket.org/MikeFarny/metarpc/issues/new) and be as specific and descriptive as possible
* Other issues, please contact the [admin](https://bitbucket.org/MikeFarny)