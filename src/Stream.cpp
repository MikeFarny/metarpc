////////////
//
// File:      Stream.cpp
// Authors:   Mike Farnsworth
// Copyright: (C) 2016 Michael Farnsworth, All Rights Reserved.  See LICENSE for more info.
// Content:   Remote procedure call interface
//
////////////

#include <cstdio>
#include <string>

#include <MetaRPC/EndianSwap.h>
#include <MetaRPC/Stream.h>
#include <MetaRPC/Procedure.h>
#include <MetaRPC/Exceptions.h>


namespace MetaRPC
{


namespace
{


// Wire protocol markers
const int32_t kStreamCookie        = 0xCAFEBEEF;
const int32_t kStreamCookieSwap    = 0xEFBEFECA;
const int32_t kProcedureCookie     = 0xFEEFF00D;
const int32_t kProcedureCookieSwap = 0x0DF0EFFE;
const int32_t kReturnCookie        = 0xCAFEF00D;
const int32_t kReturnCookieSwap    = 0x0DF0FECA;
const int32_t kCloseCookie         = 0xDEADF00D;
const int32_t kCloseCookieSwap     = 0x0DF0ADDE;


// Helper for invoking local procedures, used to search for a match for the
// requested procedure from the wire
class ComparisonProcedure : public ProcedureBase
{
public:
    ComparisonProcedure(const std::string& procName,
                        const uint32_t numArgs,
                        const DataChunk argChunks[])
        : ProcedureBase(procName), m_nArgs(numArgs)
    {
        if (numArgs > 0)
        {
            m_argTypes.resize(numArgs);
            for (uint32_t i = 0; i < numArgs; ++i)
            {
                m_argTypes[i] = argChunks[i].type();
            }
        }
    }

    virtual ~ComparisonProcedure() { }

    virtual void operator ()(DataChunk& returnChunk,
                             uint32_t numArgs,
                             const DataChunk argChunks[],
                             bool endianSwap)
    {
        // This is a no-op; we are just here to implement equals(...)
    }

    virtual uint32_t numArguments() const
    {
        return m_nArgs;
    }

    virtual DataType returnType() const
    {
        // Fudging it here; we just assume void, although this shouldn't be used
        return EncodingType<void>::TypeDesc;
    }

private:
    uint32_t m_nArgs;
};


} // namespace


Stream::Stream(Connection& underlyingConnection)
    : m_localProcedures(),
      m_connection(underlyingConnection),
      m_endianSwap(false),
      m_buffer(nullptr),
      m_bufferFilled(0),
      m_bufferSize(0),
      m_uniqueCallId(1),
      m_readiness(kNotReady)
{
    // TODO: move this to update() to avoid exceptions during construction?
    m_connection.update();
    if (m_connection.isOpen())
    {
        int32_t streamCookie = kStreamCookie;
        m_connection.write(&streamCookie, sizeof(streamCookie));
        m_readiness = kSentHandshake;
    }
}


void Stream::registerProcedure(ProcedureBase& procedure)
{
    m_localProcedures.insert(LocalProceduresMapEntry(procedure.name(), &procedure));
}


bool Stream::unregisterProcedure(ProcedureBase& procedure)
{
    bool erasedAProc = false;
    bool foundProc = false;
    do
    {
        LocalProceduresMapRange equalRangeIters =
            m_localProcedures.equal_range(procedure.name());
        for (LocalProceduresMap::iterator iter = equalRangeIters.first;
             iter != equalRangeIters.second;
             ++iter)
        {
            if (iter->second->equals(procedure))
            {
                m_localProcedures.erase(iter);
                foundProc = true;
                erasedAProc = true;
                break;
            }
        }
        foundProc = false;
    } while (foundProc);

    return erasedAProc;
}


void Stream::unregisterAllProcedures()
{
    m_localProcedures.clear();
}


bool Stream::invokeRPC(const std::string& procedureName,
                       DataChunk& returnChunk,
                       const uint32_t numArgs,
                       DataChunk argChunks[])
{
    update();

    // The process goes like thus:
    // Send out the procedure cookie, total size of the procedure call,
    // a unique ID for the call, size of the name, the name,
    // number of params, then each param chunk.
    //
    // Then, wait for a return value if the type is not void.
    // When waiting for the return value, there may be other data in the
    // pipe, so we need to spin on Update() until we see our return
    // cookie.

    int32_t procCookie = kProcedureCookie;
    if (!m_connection.write(&procCookie, sizeof(procCookie)))
    {
        hardClose();
        return false;
    }

    // Size: unique call Id, name length specifier, name length,
    // num params specifier, chunks
    int32_t totalSize = sizeof(int32_t) * 3 + procedureName.length();
    for (uint32_t i = 0; i < numArgs; ++i)
    {
        // Include data size, size specifier, and type specifier
        totalSize += argChunks[i].size() + sizeof(int32_t) + sizeof(DataType);
    }
    if (!m_connection.write(&totalSize, sizeof(totalSize)))
    {
        hardClose();
        return false;
    }

    // Write unique call ID (unique to this end of the stream; it is
    // echoed back to us when we get a return value for this call)
    // Normally this would need to go in a mutex if we were thread-safe!
    int32_t procCallId = m_uniqueCallId;
    m_uniqueCallId++;
    if (!m_connection.write(&procCallId, sizeof(procCallId)))
    {
        hardClose();
        return false;
    }

    // Write name length, name, num params
    int32_t nameSize = procedureName.length();
    if (!m_connection.write(&nameSize, sizeof(nameSize)))
    {
        hardClose();
        return false;
    }
    if (!m_connection.write(procedureName.c_str(), procedureName.length()))
    {
        hardClose();
        return false;
    }
    if (!m_connection.write(&numArgs, sizeof(numArgs)))
    {
        hardClose();
        return false;
    }

    // Write all of the parameters
    for (uint32_t i = 0; i < numArgs; i++)
    {
        if (!writeChunk(argChunks[i]))
        {
            return false;
        }
    }

    // Make sure the data goes out, or we'll be waiting a *really*
    // long time for our return value
    m_connection.flush();

    // Look for the return value if it's non-void
    if (returnChunk.type() != EncodingType<void>::TypeDesc)
    {
        bool gotReturnValue = false;
        while (!gotReturnValue)
        {
//            // We need a return value, and we can block while waiting for it.
//            int32_t returnCookiePeek;
//            m_connection.peek(&returnCookiePeek, sizeof(returnCookiePeek));

//            // If we got a disconnect while trying to grab the return, bail
//            if (!m_connection.isOpen())
//            {
//                std::ostringstream errorStream;
//                errorStream << "Connection died while collecting return value from a remote procedure call";
//                throw RemoteProcedureCallException(procedureName, errorStream.str());
//            }

            update();

            // If we got a disconnect while trying to grab the return, bail
            if (!m_connection.isOpen())
            {
                std::ostringstream errorStream;
                errorStream << "Connection died while collecting return value from a remote procedure call";
                throw RemoteProcedureCallException(procedureName, errorStream.str());
            }

            if (m_readStatus == kReturnValueWaiting)
            {
                // We got all the data; dispatch the procedure call
                int32_t offset = 0;

                // Get call ID
                int32_t returnedCallId = *reinterpret_cast<int32_t*>(m_buffer + offset);
                offset += sizeof(returnedCallId);
                endianSwap(returnedCallId, m_endianSwap);
                if (returnedCallId != procCallId)
                {
                    std::ostringstream errorStream;
                    errorStream << "Mismatched call IDs on return value, expected "
                                << procCallId << " but got " << returnedCallId;
                    throw RemoteProcedureCallException(procedureName, errorStream.str());
                }

                // Skip the chunk size specifier; it's already encoded
                // in the bufferSize
                offset += sizeof(int32_t);

                // Read the type ID of the chunk
                DataType chunkType = *reinterpret_cast<DataType*>(m_buffer + offset);
                offset += sizeof(chunkType);
                endianSwap(chunkType, m_endianSwap);

                // Read the chunk size; subtract off the data type and
                // the unique call ID specifiers size as we only care
                // about the actual return data ATM
                int32_t chunkSize = m_bufferSize - offset;

                // Package up this parameter's data
                returnChunk.setSize(chunkSize);
                returnChunk.setType(chunkType);
                returnChunk.setData(static_cast<void*>(m_buffer + offset));

                delete[] m_buffer;
                m_buffer = nullptr;
                m_bufferFilled = m_bufferSize = 0;
                m_readStatus = kReadingNone;

                gotReturnValue = true;
            }
            else
            {
                // TODO: sleep here to be nice to the CPU
            }
        }
    }

    return true;
}


void Stream::invokeLocalProcedure(const int32_t procCallId,
                                  const std::string& procedureName,
                                  const uint32_t numArgs,
                                  DataChunk argChunks[])
{
    bool returnSentAlready = false;

    ComparisonProcedure compareProc =
        ComparisonProcedure(procedureName, numArgs, argChunks);

    LocalProceduresMapRange equalRangeIters =
        m_localProcedures.equal_range(procedureName);
    for (LocalProceduresMap::iterator iter = equalRangeIters.first;
         iter != equalRangeIters.second;
         ++iter)
    {
        if (iter->second->equals(compareProc))
        {
            DataChunk returnChunk;
            (*iter->second)(returnChunk, numArgs, argChunks, m_endianSwap);

            // Only send back a return value if it's a non-void procedure,
            // and if we haven't already encountered a handler (e.g. we
            // can only ever send back the first return value if there
            // are duplicate-registered procedures of the same signature)
            if (iter->second->returnType() != static_cast<DataType>(EncodingType<void>::TypeDesc) &&
                !returnSentAlready)
            {
                sendReturnChunk(procCallId, procedureName, returnChunk);
                returnSentAlready = true;
            }
        }
    }
}


void Stream::sendReturnChunk(const int32_t procCallId,
                             const std::string& procedureName,
                             DataChunk& returnChunk)
{
    if (m_connection.isOpen())
    {
        int32_t returnCookie = kReturnCookie;
        int32_t callId = procCallId;
        if (!(m_connection.write(&returnCookie, sizeof(returnCookie)) &&
              m_connection.write(&callId, sizeof(callId)) &&
              writeChunk(returnChunk)))
        {
            // Something failed during the send
            hardClose();
            std::ostringstream errorStream;
            errorStream << "Remote side invoked local procedure \"" << procedureName
                        << "\", but the connection went away before a return value could be sent";
            throw StreamException(this, errorStream.str());
        }
        else
        {
            // Make sure the receiver gets their return value ASAP
            m_connection.flush();
        }
    }
    else
    {
        // What the heck?
        hardClose();
        std::ostringstream errorStream;
        errorStream << "Remote side invoked local procedure \"" << procedureName
                    << "\", but the connection went away before a return value could be sent";
        throw StreamException(this, errorStream.str());
    }
}


bool Stream::writeChunk(DataChunk& chunk)
{
    bool succeeded = true;
    if (chunk.size() == 0)
    {
        throw StreamException(this, "Tried to send an empty data chunk!");
    }
    if (m_connection.isOpen())
    {
        int32_t chunkSize = chunk.size();
        DataType chunkType = chunk.type();
        if (!(m_connection.write(&chunkSize, sizeof(chunkSize)) &&
              m_connection.write(&chunkType, sizeof(chunkType))))
        {
            hardClose();
            return false;
        }
        if (chunkSize > 0 && !m_connection.write(chunk.data(), chunkSize))
        {
            hardClose();
            return false;
        }
    }
    return succeeded;
}


void Stream::update()
{
    // Give the connection a chance to accept connections, etc.
    m_connection.update();

    size_t numBytesAvailable = m_connection.numBytesAvailable();

    // Sanity check
    if (!m_connection.isOpen())
    {
        m_readiness = kNotReady;
        m_readStatus = kReadingNone;
        return;
    }
    
    if (m_readiness == kNotReady)
    {
        int32_t streamCookie = kStreamCookie;
        m_connection.write(&streamCookie, sizeof(streamCookie));
        m_readiness = kSentHandshake;
    }

    // Decide how much we want to poll for
    size_t numBytesExpected;
    if (m_buffer == nullptr || m_bufferSize <= m_bufferFilled)
    {
        // No pending procedure calls, so we're just looking for
        // a cookie of some kind (stream, or procedure)
        numBytesExpected = sizeof(int32_t);
    }
    else
    {
        // We are filling a buffer in preparation for a procedure call.
        // Just keep filling it as needed.
        numBytesExpected = size_t(m_bufferSize - m_bufferFilled);
    }

    enum UpdateError
    {
        kNoError,
        kProtocolMismatchStream,
        kProtocolMismatchProcedure,
        kConnectionDied
    };
    UpdateError currentError = kNoError;
    bool bail = false;
    bool disconnect = false;

    while (numBytesAvailable > 0 && !bail)
    {
        if (m_readiness != kReceivedHandshake)
        {
            // Stream is not ready, we're looking for a handshake
            if (numBytesAvailable >= sizeof(int32_t))
            {
                // Read the stream cookie to see if we're in business
                int32_t streamCookie = 0;
                m_connection.read(&streamCookie, sizeof(streamCookie));
                if (streamCookie == kStreamCookie)
                {
                    m_readiness = kReceivedHandshake;
                }
                else if (streamCookie == kStreamCookieSwap)
                {
                    m_readiness = kReceivedHandshake;
                    m_endianSwap = true;
                }
                else if (streamCookie == kCloseCookie ||
                         streamCookie == kCloseCookieSwap)
                {
                    disconnect = true;
                    break;
                }
                else
                {
                    bail = true;
                    currentError = kProtocolMismatchStream;
                    break;
                }
            }
        }
        else
        {
            // We already established a connection

            // Make sure the buffer exists (this should be extraneous!)
            /* if (m_buffer == nullptr && m_bufferSize > 0)
            {
                m_buffer = new char[m_bufferSize];
            }
            else */ if (m_buffer == nullptr)
            {
                // See if we have an incoming call or return value

                // We're looking for the procedure call cookie and size;
                // but there might be a return value chunk waiting.
                // Check for that first, and exit the update if one is
                // waiting (to give an invoker a chance at it).
                m_readStatus = kReadingNone;
                if (numBytesAvailable >= sizeof(int32_t))
                {
                    int32_t pendingCookie = 0;
                    m_connection.peek(&pendingCookie, sizeof(pendingCookie));
                    endianSwap(pendingCookie, m_endianSwap);
                    if (pendingCookie == kCloseCookie)
                    {
                        disconnect = true;
                        break;
                    }
                    else if (pendingCookie == kReturnCookie)
                    {
                        m_readStatus = kReadingReturnValue;
                    }
                    else if (pendingCookie == kProcedureCookie)
                    {
                        m_readStatus = kReadingProcedureCall;
                    }
                    else
                    {
                        bail = true;
                        currentError = kProtocolMismatchProcedure;
                        break;
                    }
                }

                // Get header data for whatever is waiting

                if (m_readStatus == kReadingReturnValue &&
                    numBytesAvailable >= sizeof(int32_t) * 3)
                {
                    int32_t returnCookie = 0;
                    m_connection.read(&returnCookie, sizeof(returnCookie));
                    endianSwap(returnCookie, m_endianSwap);
                    if (returnCookie != kReturnCookie)
                    {
                        bail = true;
                        currentError = kProtocolMismatchProcedure;
                        break;
                    }
                    // Note we don't endian-swap the proc id; we let
                    // the handler handle that.  We just write it back
                    // into the buffer for them to deal with if they
                    // want to.
                    int32_t returnedProcId = 0;
                    if (!m_connection.read(&returnedProcId, sizeof(returnedProcId)))
                    {
                        bail = true;
                        currentError = kConnectionDied;
                        break;
                    }
                    if (!m_connection.read(&m_bufferSize, sizeof(m_bufferSize)))
                    {
                        m_bufferSize = 0;
                        bail = true;
                        currentError = kConnectionDied;
                        break;
                    }
                    endianSwap(m_bufferSize, m_endianSwap);

                    // Size is only the actual return chunk data size,
                    // so we have to adjust for the proc call ID,
                    // the chunk size specifier, and data type specifier
                    m_buffer = new char[m_bufferSize + sizeof(DataType) + sizeof(int32_t) * 2];
                    std::memcpy(m_buffer, &returnedProcId, sizeof(returnedProcId));
                    std::memcpy(&m_buffer[sizeof(int32_t)],
                                &m_bufferSize, sizeof(m_bufferSize));
                    m_bufferFilled = sizeof(int32_t) * 2;
                    numBytesAvailable = m_connection.numBytesAvailable();
                    // Only gotta read what's left to complete the buffer
                    numBytesExpected = m_bufferSize + sizeof(DataType);
                    m_bufferSize += sizeof(DataType) + sizeof(int32_t) * 2;
                }
                else if (m_readStatus == kReadingProcedureCall &&
                         numBytesAvailable >= sizeof(int32_t) * 2)
                {
                    int32_t procCookie = 0;
                    m_connection.read(&procCookie, sizeof(procCookie));
                    endianSwap(procCookie, m_endianSwap);
                    if (procCookie != kProcedureCookie)
                    {
                        bail = true;
                        currentError = kProtocolMismatchProcedure;
                        break;
                    }
                    if (!m_connection.read(&m_bufferSize, sizeof(m_bufferSize)))
                    {
                        bail = true;
                        currentError = kConnectionDied;
                        break;
                    }
                    endianSwap(m_bufferSize, m_endianSwap);
                    // Set up the buffer for the procedure call data
                    m_buffer = new char[m_bufferSize];
                    m_bufferFilled = 0;
                    numBytesAvailable = m_connection.numBytesAvailable();
                    numBytesExpected = m_bufferSize;
                }
            }

            if (m_buffer != nullptr && m_bufferSize > 0 && numBytesAvailable > 0)
            {
                // We have a buffer, let's try to read as much data
                // as we can, up to the size of the procedure call data.
                uint32_t bytesToRead = numBytesAvailable < numBytesExpected ?
                    numBytesAvailable : numBytesExpected;
                if (m_connection.read(&m_buffer[m_bufferFilled], bytesToRead))
                {
                    m_bufferFilled += bytesToRead;
                    numBytesExpected -= bytesToRead;
                }
                else
                {
                    bail = true;
                    currentError = kConnectionDied;
                    break;
                }

                if (m_bufferFilled == m_bufferSize)
                {
                    if (m_readStatus == kReadingProcedureCall)
                    {
                        // We got all the data; dispatch the procedure call
                        int32_t offset = 0;

                        // Read the procedure name size
                        int32_t procCallId = *reinterpret_cast<int32_t*>(m_buffer + offset);
                        offset += sizeof(procCallId);
                        endianSwap(procCallId, m_endianSwap);

                        // Read the procedure name size
                        int32_t procNameSize = *reinterpret_cast<int32_t*>(m_buffer + offset);
                        offset += sizeof(procNameSize);
                        endianSwap(procNameSize, m_endianSwap);
                        std::string procName = std::string(m_buffer + offset, procNameSize);
                        offset += procNameSize;

                        // Read the number of arguments
                        int32_t numProcArgs = *reinterpret_cast<int32_t*>(m_buffer + offset);
                        offset += sizeof(numProcArgs);
                        endianSwap(numProcArgs, m_endianSwap);

                        // Grab each parameter chunk
                        DataChunk *argChunks = nullptr;
                        if (numProcArgs > 0)
                        {
                            argChunks = new DataChunk[numProcArgs];
                        }

                        for (int32_t i = 0; i < numProcArgs; ++i)
                        {
                            // Read the chunk size
                            int32_t chunkSize = *reinterpret_cast<int32_t*>(m_buffer + offset);
                            offset += sizeof(chunkSize);
                            endianSwap(chunkSize, m_endianSwap);

                            // Read the type ID of the chunk
                            DataType chunkType = *reinterpret_cast<DataType*>(m_buffer + offset);
                            offset += sizeof(chunkType);
                            endianSwap(chunkType, m_endianSwap);

                            // Package up this parameter's data
                            argChunks[i].setSize(chunkSize);
                            argChunks[i].setType(chunkType);
                            argChunks[i].setData(static_cast<void*>(m_buffer + offset));
                            offset += chunkSize;
                        }

                        invokeLocalProcedure(procCallId,
                                             procName,
                                             numProcArgs,
                                             argChunks);

                        delete[] argChunks;
                        delete[] m_buffer;
                        m_buffer = nullptr;
                        m_bufferFilled = m_bufferSize = 0;
                        m_readStatus = kReadingNone;
                    }
                    else if (m_readStatus == kReadingReturnValue)
                    {
                        // Let the invoker handle the return chunk data
                        m_readStatus = kReturnValueWaiting;
                        break;
                    }
                }
            }
        }
        // Prep for the next time through
        numBytesAvailable = m_connection.numBytesAvailable();
    }

    if (bail)
    {
        m_readStatus = kReadingNone;

        const char *errorDesc;
        switch (currentError)
        {
        case kConnectionDied:
            errorDesc = "the connection closed";
            break;

        case kProtocolMismatchStream:
            errorDesc = "the data stream is not a MetaRPC stream";
            break;

        case kProtocolMismatchProcedure:
            errorDesc = "the stream failed to find a procedure or return value token";
            break;

        default:
            errorDesc = "of an unknown error";
            break;
        }
        m_readiness = kNotReady;
        hardClose();
        throw StreamException(this, std::string("Stream closed because: ") + errorDesc);
    }
    else if (disconnect)
    {
        m_readiness = kNotReady;
        hardClose();
    }
}


void Stream::close()
{
    if (m_connection.isOpen())
    {
        int32_t closeCookie = kCloseCookie;
        m_connection.write(&closeCookie, sizeof(closeCookie));
        
        m_connection.flush();

        hardClose();
    }
    m_readiness = kNotReady;
}


void Stream::hardClose()
{
    delete[] m_buffer;
    m_buffer = nullptr;
    m_bufferFilled = m_bufferSize = 0;
    m_connection.close();
}


void Stream::cloneRegisteredProcedures(Stream& destStream)
{
    for (LocalProceduresMap::iterator iter = m_localProcedures.begin();
         iter != m_localProcedures.end();
         ++iter)
    {
        destStream.registerProcedure(*(*iter).second);
    }
}


} // namespace MetaRPC
