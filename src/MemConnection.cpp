////////////
//
// File:      MemConnection.h
// Authors:   Mike Farnsworth
// Copyright: (C) 2016 Michael Farnsworth, All Rights Reserved.  See LICENSE for more info.
// Content:   Remote procedure call shared-memory connection.
//
////////////

#include <cstdio>
#include <cstring>
#include <algorithm>

#include <MetaRPC/Exceptions.h>
#include <MetaRPC/MemConnection.h>


namespace MetaRPC
{


MemConnection::MemConnection(size_t bufferSize)
    : Connection(),
      m_remote(nullptr),
      m_buffer(nullptr),
      m_writeIndex(0),
      m_readIndex(0),
      m_size(bufferSize),
      m_rwMutex()
{
    m_buffer = new unsigned char[m_size];
    m_isOpen = true;
}


MemConnection::~MemConnection()
{
    close();
    delete[] m_buffer;
}


bool MemConnection::write(const void *data, size_t numBytes)
{
    if (!m_remote)
    {
        return false;
    }

    bool overrun = false;
    {
        boost::mutex::scoped_lock lock(m_remote->m_rwMutex);

        const unsigned char *charBuffer = reinterpret_cast<const unsigned char *>(data);
        if (numBytes + m_remote->m_writeIndex < m_remote->m_size)
        {
            if (m_remote->m_readIndex <= m_remote->m_writeIndex ||
                numBytes + m_remote->m_writeIndex <= m_remote->m_readIndex)
            {
                std::memcpy(&m_remote->m_buffer[m_remote->m_writeIndex],
                            charBuffer,
                            numBytes);
                m_remote->m_writeIndex += numBytes;
            }
            else
            {
                overrun = true;
//                std::ostringstream errorStream;
//                errorStream << "Memory connection tried to write but would overrun the buffer, size " << m_size;
//                throw ConnectionException(nullptr, errorStream.str());
            }
        }
        else
        {
            size_t numBytesPhase1 = m_remote->m_size - m_remote->m_writeIndex;
            size_t numBytesPhase2 = numBytes - numBytesPhase1;
            std::memcpy(&m_remote->m_buffer[m_remote->m_writeIndex],
                        charBuffer,
                        numBytesPhase1);
            if (numBytesPhase2 > 0)
            {
                std::memcpy(m_remote->m_buffer,
                            &charBuffer[numBytesPhase1],
                            numBytesPhase2);
            }
            m_remote->m_writeIndex = numBytesPhase2;
        }
    }
    return !overrun;
}


bool MemConnection::read(void *data, size_t numBytes)
{
    if (numBytesAvailable() < numBytes)
    {
//        std::ostringstream errorStream;
//        errorStream << "Memory connection tried to read more than what is available";
//        throw ConnectionException(nullptr, errorStream.str());
        return false;
    }

    {
        boost::mutex::scoped_lock lock(m_rwMutex);

        unsigned char *charBuffer = reinterpret_cast<unsigned char *>(data);
        if (m_readIndex < m_writeIndex)
        {
            std::memcpy(charBuffer, &m_buffer[m_readIndex], numBytes);
            m_readIndex += numBytes;
        }
        else
        {
            size_t numBytesPhase1 = std::min(m_size - m_readIndex, numBytes);
            size_t numBytesPhase2 = numBytes - numBytesPhase1;
            std::memcpy(charBuffer, &m_buffer[m_readIndex], numBytesPhase1);
            if (numBytesPhase2 > 0)
            {
                std::memcpy(&charBuffer[numBytesPhase1], m_buffer, numBytesPhase2);
            }
            if (m_size - m_readIndex > numBytes)
            {
                m_readIndex += numBytes;
            }
            else
            {
                m_readIndex = numBytesPhase2;
            }
        }
    }
    return true;
}


bool MemConnection::peek(void *data, size_t numBytes)
{
    if (numBytesAvailable() < numBytes)
    {
        // Just return the status; error messages here are not needed
        return false;
    }

    {
        boost::mutex::scoped_lock lock(m_rwMutex);

        unsigned char *charBuffer = reinterpret_cast<unsigned char *>(data);
        if (m_readIndex < m_writeIndex)
        {
            std::memcpy(charBuffer, &m_buffer[m_readIndex], numBytes);
        }
        else
        {
            size_t numBytesPhase1 = std::min(m_size - m_readIndex, numBytes);
            size_t numBytesPhase2 = numBytes - numBytesPhase1;
            std::memcpy(charBuffer, &m_buffer[m_readIndex], numBytesPhase1);
            if (numBytesPhase2 > 0)
            {
                std::memcpy(&charBuffer[numBytesPhase1], m_buffer, numBytesPhase2);
            }
        }
    }
    return true;
}


} // namespace MetaRPC
