////////////
//
// File:      SocketConnection.cpp
// Authors:   Mike Farnsworth
// Copyright: (C) 2016 Michael Farnsworth, All Rights Reserved.  See LICENSE for more info.
// Content:   Remote procedure call networked socket connection.
//
////////////

#include <cstdio>
#include <cstring>
#include <string>
#include <sstream>

#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <unistd.h>

#include <MetaRPC/Exceptions.h>
#include <MetaRPC/SocketConnection.h>


namespace MetaRPC
{


namespace
{

const int kInvalidSocket = -1;

}


SocketConnection::SocketConnection(const std::string& connectDest,
                                   bool listenForConnections,
                                   SocketFlavor socketFlavor,
                                   int connectPort,
                                   bool noWait)
    : Connection(),
      m_socketFD(kInvalidSocket),
      m_clientSocketFD(kInvalidSocket),
      m_port(connectPort),
      m_noDelay(noWait),
      m_listenMode(listenForConnections),
      m_remote(connectDest),
      m_flavor(socketFlavor)
{
    if (m_flavor == kIPv4Socket)
    {
        m_socketFD = ::socket(AF_INET, SOCK_STREAM, 0);
    }
    else if (m_flavor == kIPv6Socket)
    {
        m_socketFD = ::socket(AF_INET6, SOCK_STREAM, 0);
    }
    else if (m_flavor == kUnixDomainSocket)
    {
        m_socketFD = ::socket(AF_LOCAL, SOCK_STREAM, 0);
    }
    else
    {
        throw ConnectionException(nullptr, "Invalid socket flavor specified");
    }

    if (m_socketFD == kInvalidSocket)
    {
        std::ostringstream errorStream;
        errorStream << "Cannot create socket of type ";
        if (m_flavor == kIPv4Socket)
            errorStream << "IPv4";
        else if (m_flavor == kIPv6Socket)
            errorStream << "IPv6";
        else
            errorStream << "UNIX domain socket";
        throw ConnectionException(nullptr, errorStream.str());
    }

    if ((m_flavor == kIPv4Socket || m_flavor == kIPv6Socket) && m_noDelay)
    {
        int opt = 1;
        if (::setsockopt(m_socketFD,
                         SOL_TCP,
                         TCP_NODELAY,
                         (char*)&opt,
                         sizeof(opt)) < 0)
        {

            ::close(m_socketFD);
            throw ConnectionException(nullptr, "Socket could not activate TCP_NODELAY");
        }
    }

    if (m_listenMode)
    {
        if (m_flavor == kIPv4Socket || m_flavor == kIPv6Socket)
        {
            sockaddr_in serverAddr;
            std::memset(&serverAddr, 0, sizeof(serverAddr));
            serverAddr.sin_family = (m_flavor == kIPv4Socket) ? AF_INET : AF_INET6;
            serverAddr.sin_addr.s_addr = INADDR_ANY;
            serverAddr.sin_port = htons(m_port);
            if (::bind(m_socketFD,
                       (sockaddr *)&serverAddr,
                       sizeof(serverAddr)) < 0)
            {
                ::close(m_socketFD);
                std::ostringstream errorStream;
                errorStream << "Socket could not bind to port " << m_port;
                throw ConnectionException(nullptr, errorStream.str());
            }
            if (::listen(m_socketFD, 5) < 0)
            {
                ::close(m_socketFD);
                std::ostringstream errorStream;
                errorStream << "Socket could not listen on port " << m_port;
                throw ConnectionException(nullptr, errorStream.str());
            }
        }
        else if (m_flavor == kUnixDomainSocket)
        {
            sockaddr_un serverAddr;
            std::memset(&serverAddr, 0, sizeof(serverAddr));
            serverAddr.sun_family = AF_LOCAL;
            std::strcpy(serverAddr.sun_path, m_remote.c_str());
            size_t size = std::strlen(serverAddr.sun_path) + sizeof(serverAddr.sun_family);
            if (::bind(m_socketFD, (sockaddr *)&serverAddr, (int)size) < 0)
            {
                ::close(m_socketFD);
                std::ostringstream errorStream;
                errorStream << "Listener could not bind to UNIX domain socket: " << serverAddr.sun_path;
                throw ConnectionException(nullptr, errorStream.str());
            }
            if (::listen(m_socketFD, 5) < 0)
            {
                ::unlink(serverAddr.sun_path);
                ::close(m_socketFD);
                std::ostringstream errorStream;
                errorStream << "Listener could not listen on UNIX domain socket: " << serverAddr.sun_path;
                throw ConnectionException(nullptr, errorStream.str());
            }
        }
    }
    else
    {
        if (m_flavor == kIPv4Socket || m_flavor == kIPv6Socket)
        {
            hostent *server = ::gethostbyname(m_remote.c_str());
            sockaddr_in serverAddr;
            std::memset(&serverAddr, 0, sizeof(serverAddr));
            serverAddr.sin_family = (m_flavor == kIPv4Socket) ? AF_INET : AF_INET6;
            std::memcpy(&serverAddr.sin_addr.s_addr,
                        server->h_addr,
                        server->h_length);
            serverAddr.sin_port = htons(m_port);
            if (::connect(m_socketFD,
                          (sockaddr *)&serverAddr,
                          sizeof(serverAddr)) < 0)
            {
                ::close(m_socketFD);
                std::ostringstream errorStream;
                errorStream << "Client socket could not connect to host: " << m_remote << ':' << m_port;
                throw ConnectionException(nullptr, errorStream.str());
            }
            m_clientSocketFD = m_socketFD;
            m_isOpen = true;
        }
        else if (m_flavor == kUnixDomainSocket)
        {
            sockaddr_un serverAddr;
            std::memset(&serverAddr, 0, sizeof(serverAddr));
            serverAddr.sun_family = AF_LOCAL;
            std::strcpy(serverAddr.sun_path, m_remote.c_str());
            size_t size = std::strlen(serverAddr.sun_path) + sizeof(serverAddr.sun_family);
            if (::connect(m_socketFD, (sockaddr *)&serverAddr, (int)size) < 0)
            {
                ::close(m_socketFD);
                std::ostringstream errorStream;
                errorStream << "Client could not connect to UNIX domain socket: " << serverAddr.sun_path;
                throw ConnectionException(nullptr, errorStream.str());
            }
            m_clientSocketFD = m_socketFD;
            m_isOpen = true;
        }
    }
}


SocketConnection::~SocketConnection()
{
    // Shut down the client connection, if any
    close();

    // Shut down the listener if it is open
    if (m_listenMode && m_socketFD != kInvalidSocket)
    {
        ::shutdown(m_socketFD, SHUT_RDWR);
        ::close(m_socketFD);
        if (m_flavor == kUnixDomainSocket)
        {
            ::unlink(m_remote.c_str());
        }
   }
}


void SocketConnection::update()
{
    if (m_listenMode && !m_isOpen && m_socketFD != kInvalidSocket)
    {
        pollfd pollFD;
        std::memset(&pollFD, 0, sizeof(pollfd));
        pollFD.fd = m_socketFD;
        pollFD.events = POLLIN | POLLPRI;
        int pollResult = ::poll(&pollFD, 1, 30);
        if (pollResult > 0)
        {
            if (m_flavor == kIPv4Socket || m_flavor == kIPv6Socket)
            {
                sockaddr_in clientAddr;
                int clientAddrlen = sizeof(clientAddr);
                m_clientSocketFD = ::accept(m_socketFD,
                                            (sockaddr *)&clientAddr,
                                            (socklen_t *)&clientAddrlen);
                if (m_clientSocketFD != kInvalidSocket)
                {
                    // We got a valid connection
                    if (m_noDelay &&
                        (m_flavor == kIPv4Socket || m_flavor == kIPv6Socket))
                    {
                        int opt = 1;
                        if (::setsockopt(m_clientSocketFD,
                                         SOL_TCP,
                                         TCP_NODELAY,
                                         (char*)&opt,
                                         sizeof(opt)) < 0)
                        {
                            close();
                            throw ConnectionException(nullptr, "Listener socket could not activate TCP_NODELAY for new client");
                        }
                    }
                    m_isOpen = true;
                }
            }
            else if (m_flavor == kUnixDomainSocket)
            {
                sockaddr_un clientAddr;
                int clientAddrLen = sizeof(clientAddr);
                m_clientSocketFD = ::accept(m_socketFD,
                                            (sockaddr *)&clientAddr,
                                            (socklen_t *)&clientAddrLen);
                if (m_clientSocketFD != kInvalidSocket)
                {
                    m_isOpen = true;
                }
            }
        }
        else if (pollResult < 0)
        {
            // Somefin broke
            ::shutdown(m_socketFD, SHUT_RDWR);
            ::close(m_socketFD);
            m_socketFD = kInvalidSocket;
            if (m_flavor == kUnixDomainSocket)
            {
                ::unlink(m_remote.c_str());
            }
            std::ostringstream errorStream;
            errorStream << "Listener socket could not poll on port " << m_port;
            throw ConnectionException(nullptr, errorStream.str());
        }
    }

    if (m_isOpen && m_clientSocketFD != kInvalidSocket)
    {
        // Check the health of the connection; all we need to do is
        // check if available data is waiting (which might catch a
        // disconnection), and if some is waiting, then we can actually
        // do a peek recv on it and check that return value as well;
        // that should catch further disconnection status.  The following
        // two function calls will set the disconnection status as
        // needed.
        if (numBytesAvailable() > 0)
        {
            char throwAwayValue;
            peek(&throwAwayValue, 1);
        }
    }
}


bool SocketConnection::write(const void *data, size_t numBytes)
{
    if (m_isOpen && m_clientSocketFD != kInvalidSocket)
    {
        // It's possible for send() to not get all the data out the door;
        // we loop and ram the data down its mouth (essentially this is
        // a blocking send(), and it only exits early on error).
        ssize_t sent = 0;
        const char *sendData = reinterpret_cast<const char *>(data);
        while (sent < (ssize_t)numBytes)
        {
            // Send the actual data.  Note that we explicitly forbid broken
            // pipe signals, as we check the results for errors and close the
            // connection if there is an issue.  A signal would just cause an
            // abort, which is a cruddy thing to do to the app.
            ssize_t sendResult =
                ::send(m_clientSocketFD, sendData, numBytes - sent, MSG_NOSIGNAL);
            if (sendResult < 0)
            {
                // Send failed; we aren't connected anymore.
                // Clean up client connection only, so a reconnect is
                // possible later.
                close();
                return false;
            }
            sent += sendResult;
            sendData += sendResult;
        }
        return true;
    }
    return false;
}


bool SocketConnection::read(void *data, size_t numBytes)
{
    if (m_isOpen && m_clientSocketFD != kInvalidSocket)
    {
        ssize_t received = ::recv(m_clientSocketFD,
                                  data,
                                  numBytes,
                                  MSG_WAITALL);
        if (received <= 0)
        {
            // We got a disconnection; so tear down the client properly.
            // Note we don't tear down the listener; this allows reconnects.
            close();
            return false;
        }
        if (received < (ssize_t)numBytes)
        {
            // We read something, but not enough; we might have a lost connection?
            return false;
        }
        return true;
    }
    return false;
}


bool SocketConnection::peek(void *data, size_t numBytes)
{
    if (m_isOpen && m_clientSocketFD != kInvalidSocket)
    {
        ssize_t received = ::recv(m_clientSocketFD,
                                  data,
                                  numBytes,
                                  MSG_WAITALL | MSG_PEEK);
        if (received <= 0)
        {
            // We got a disconnection; so tear down the client properly.
            // Note we don't tear down the listener; this allows reconnects.
            close();
            return false;
        }
        return received >= (ssize_t)numBytes;
    }
    return false;
}


size_t SocketConnection::numBytesAvailable()
{
    if (m_isOpen && m_clientSocketFD != kInvalidSocket)
    {
        // Linux, Solaris, and *BSD all expect an int as the save location
        // for the ioctl call, while IRIX expects a full size_t.  We'll use
        // a size_t so that we can fit the result of either.
        size_t numBytesAvailable = 0;
        // Does this give the right amount available on unix domain sockets?
        if (::ioctl(m_clientSocketFD,
                    FIONREAD,
                    reinterpret_cast<char *>(&numBytesAvailable)) < 0)
        {
            // Count this as a disconnection, and tear down the client.
            // Note we don't tear down the listener; this allows reconnects.
            close();
            return 0;
        }
        return numBytesAvailable;
    }
    return 0;
}


void SocketConnection::close()
{
    if (m_clientSocketFD != kInvalidSocket)
    {
        ::shutdown(m_clientSocketFD, SHUT_RDWR);
        ::close(m_clientSocketFD);
        m_clientSocketFD = kInvalidSocket;
        if (!m_listenMode)
        {
            // This was the same as clientSocketFD, so zero it out too
            m_socketFD = kInvalidSocket;
        }
    }
    m_isOpen = false;
}


} // namespace MetaRPC
