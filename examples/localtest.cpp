////////////
//
// File:      localtest.cpp
// Authors:   Mike Farnsworth
// Copyright: (C) 2016 Michael Farnsworth, All Rights Reserved.  See LICENSE for more info.
// Content:   MetaRPC transport/call tests
//
////////////

#include <cstdio>
#include <cstring>
#include <string>

#include <boost/thread.hpp>
#include <boost/thread/xtime.hpp>

#include <MetaRPC/Procedure.h>
#include <MetaRPC/Stream.h>
#include <MetaRPC/SocketConnection.h>
#include <MetaRPC/MemConnection.h>


using namespace MetaRPC;


const int kNumTries = 20;

bool gVerbose = false;


void impl1_1(int a, int b)
{
    if (gVerbose)
    {
        std::printf("Result inside Proc1_1: %d + %d = %d\n", a, b, a + b);
        std::fflush(stdout);
    }
}

void impl1_2(int a, int b)
{
    if (gVerbose)
    {
        std::printf("Result inside Proc1_2: %d * %d = %d\n", a, b, a * b);
        std::fflush(stdout);
    }
}

int impl2(int a, int b)
{
    return a + b;
}


boost::mutex gStatusMutex;
bool gDone = false;
int gThreadsDone = 0;


class RPCTestThread1
{
public:
    RPCTestThread1(Stream& stream, const std::string& desc)
        : m_stream(stream), m_description(desc) { }

    void operator ()()
    {
        Procedure< int (int, int) > procType2("Proc2");
        procType2.signal().connect(&impl2);

        m_stream.registerProcedure(procType2);

        RemoteProcedure< void (int, int) > remoteProc1(m_stream, "Proc1");

        int numTries = kNumTries;

        if (gVerbose)
            fprintf(stdout, "Starting thread 1 (%s)\n", m_description.c_str());

        while (m_stream.connection().isOpen())
        {
            m_stream.update();
            if (numTries > 0)
            {
                remoteProc1(numTries, 5);
                numTries--;
            }
            if (numTries <= 0)
                break;
        }
        
        {
            boost::mutex::scoped_lock lock(gStatusMutex);
            gThreadsDone++;
        }
        while (true)
        {
            // Keep feeding return value to the other end of the connection
            m_stream.update();
            boost::mutex::scoped_lock lock(gStatusMutex);
            if (gDone)
                break;
        }

        if (gVerbose)
            fprintf(stdout, "Ending thread 1 (%s)\n", m_description.c_str());
    }

    Stream& m_stream;
    std::string m_description;
};


class RPCTestThread2
{
public:
    RPCTestThread2(Stream& stream, const std::string& desc)
        : m_stream(stream), m_description(desc) { }

    void operator ()()
    {
        Procedure< void (int, int) > procType1("Proc1");
        procType1.signal().connect(&impl1_1);
        procType1.signal().connect(&impl1_2);

        m_stream.registerProcedure(procType1);

        RemoteProcedure< int (int, int) > remoteProc2(m_stream, "Proc2");

        int numTries = kNumTries;

        if (gVerbose)
            fprintf(stdout, "Starting thread 2 (%s)\n", m_description.c_str());

        while (m_stream.connection().isOpen())
        {
            m_stream.update();
            if (numTries > 0)
            {
                int result = remoteProc2(20, numTries);
                if (gVerbose)
                    printf("Result from Proc2: %d (%s)\n", result, m_description.c_str());
                std::fflush(stdout);
                numTries--;
            }
            if (numTries <= 0)
                break;
        }
        
        {
            boost::mutex::scoped_lock lock(gStatusMutex);
            gThreadsDone++;
        }
        while (true)
        {
            // Keep feeding return value to the other end of the connection
            m_stream.update();
            boost::mutex::scoped_lock lock(gStatusMutex);
            if (gDone)
                break;
        }

        if (gVerbose)
            fprintf(stdout, "Ending thread 2 (%s)\n", m_description.c_str());
    }

    Stream& m_stream;
    std::string m_description;
};


int main(int argc, char *argv[])
{
    //
    // MetaRPCTest: creates 3 kinds of RPC connections, and attaches a thread
    //     pair to each that will talk to each other via RPC.  The thread pair
    //     creates a local procedure to be called, and a remote procedure to
    //     call, and a stream that uses the connection to issue the RPC calls.
    //     If all goes well, results should be printing from all of the threads
    //     about the successful RPC calls.
    //

    if (argc > 1 && (!strcmp(argv[1], "-v") || !strcmp(argv[1], "--verbose")))
        gVerbose = true;

    MemConnection connection1;
    MemConnection connection2;
    connection1.setRemote(connection2);
    connection2.setRemote(connection1);

    SocketConnection netConnection1("localhost", true);
    SocketConnection netConnection2("localhost");

    // Generate some random characters for the socket name so we don't have a collision
    // (we can't connect to the socket if the socket file exists already).
    std::string socketName = "/tmp/mrpc-test-";
    boost::xtime time;
    boost::xtime_get(&time, boost::TIME_UTC_);
    srand(time.sec * 1000 + time.nsec / 1000000);
    for (int i = 0; i < 12; i++)
    {
        int randChar = rand() % 62;
        if (randChar < 26)
            randChar += 'A';
        else if (randChar < 52)
            randChar = randChar - 26 + 'a';
        else
            randChar = randChar - 52 + '0';
        socketName += char(randChar);
    }
    if (gVerbose)
        fprintf(stdout, "Using UNIX domain socket: %s\n", socketName.c_str());
    SocketConnection unixConnection1(socketName, true, SocketConnection::kUnixDomainSocket);
    SocketConnection unixConnection2(socketName, false, SocketConnection::kUnixDomainSocket);

    Stream memStream1(connection1);
    Stream memStream2(connection2);

    RPCTestThread1 testThread1Obj(memStream1, "Mem buffer 1");
    RPCTestThread2 testThread2Obj(memStream2, "Mem buffer 2");

    Stream netStream1(netConnection1);
    Stream netStream2(netConnection2);

    RPCTestThread1 testThread3Obj(netStream1, "TCP/IP listener");
    RPCTestThread2 testThread4Obj(netStream2, "TCP/IP client");

    Stream unixStream1(unixConnection1);
    Stream unixStream2(unixConnection2);

    RPCTestThread1 testThread5Obj(unixStream1, "UDS listener");
    RPCTestThread2 testThread6Obj(unixStream2, "UDS client");

    boost::thread thread1(testThread1Obj);
    boost::thread thread2(testThread2Obj);
    boost::thread thread3(testThread3Obj);
    boost::thread thread4(testThread4Obj);
    boost::thread thread5(testThread5Obj);
    boost::thread thread6(testThread6Obj);
    
    while (true)
    {
        boost::xtime time;
        boost::xtime_get(&time, boost::TIME_UTC_);
        // One millisecond
        time.nsec += 1000000;
//        time.sec += 1;
        boost::thread::sleep(time);
        
        boost::mutex::scoped_lock lock(gStatusMutex);
        if (gThreadsDone >= 6)
        {
            gDone = true;
            break;
        }
    }

    thread1.join();
    thread2.join();
    thread3.join();
    thread4.join();
    thread5.join();
    thread6.join();

    memStream1.close();
    memStream2.close();
    netStream1.close();
    netStream2.close();
    unixStream1.close();
    unixStream2.close();

    fflush(stdout);
    fflush(stderr);

    return 0;
}
