////////////
//
// File:      conn_test.cpp
// Authors:   Mike Farnsworth
// Copyright: (C) 2016 Michael Farnsworth, All Rights Reserved.  See LICENSE for more info.
// Content:   Connection/disconnection handling test for MetaRPC.
//
////////////


#include <iostream>
#include <string>

#include <boost/thread.hpp>
#include <boost/thread/xtime.hpp>

#include <MetaRPC/Procedure.h>
#include <MetaRPC/Stream.h>
#include <MetaRPC/SocketConnection.h>
#include <MetaRPC/MemConnection.h>


using namespace MetaRPC;


bool gIsServer = false;
int gNumInvocations = 0;

const int kNumIterations = 2;


int proc()
{
	// We got called, and are connected
	// Note: no need to thread lock here, because the only way this gets called
	// is via an RPCStream::Update() which happens in serial with the rest of
	// the test.
	gNumInvocations++;

	if (gIsServer)
	{
        std::cout << "Server called" << std::endl;
		return 1;
	}
	else
	{
        std::cout << "Client called" << std::endl;
		return 2;
	}
}


void spinStream(Stream *pStream, bool updateStream = true)
{
    for (int i = 0; i < 2; i++)
    {
        if (updateStream)
        {
            pStream->update();
        }

        // Be nice to the CPU
        boost::xtime time;
        boost::xtime_get(&time, boost::TIME_UTC_);
        // 100 milliseconds
//        time.sec += 1;
        time.nsec += 100000000;
        boost::thread::sleep(time);
    }
}


int main(int argc, char *argv[])
{
	if (fork() == 0)
    {
		gIsServer = false;
    }
	else
    {
		gIsServer = true;
    }

	SocketConnection *pConnection;
	if (gIsServer)
    {
		pConnection = new SocketConnection("localhost", true);
        std::cout << "Server listening..." << std::endl;
    }
    else
	{
		// Sleep before trying to connect; this gives the main fork a chance
		// to set up the server's listener first.
		boost::xtime time;
		boost::xtime_get(&time, boost::TIME_UTC_);
//        time.sec += 1;
        time.nsec += 50000000;
		boost::thread::sleep(time);

        pConnection = new SocketConnection("localhost");
        std::cout << "Client connecting...";

        if (!pConnection->isOpen())
        {
            std::cout << "failed to connect!" << std::endl;
        }
        else
        {
            std::cout << "success!" << std::endl;
        }
    }

	Stream *pStream = new Stream(*pConnection);

	RemoteProcedure< int () > *pRemoteProc =
		new RemoteProcedure< int () >(*pStream, "Proc");

	Procedure< int () > localProc("Proc");
	localProc.signal().connect(&proc);
	pStream->registerProcedure(localProc);

	int numReconnections = 0;
	int failedIterations = 0;

	while (gNumInvocations < kNumIterations && failedIterations < 20)
	{
		int lastKnownInvocations = gNumInvocations;

		pStream->update();

		if (pConnection->isOpen())
		{
		    if (gIsServer)
		    {
		        if (lastKnownInvocations < gNumInvocations)
		        {
		            int result = (*pRemoteProc)();
                    std::cout << "Server got " << result << " from client" << std::endl;
		        }
		    }
		    else
		    {
		        int result = (*pRemoteProc)();
                std::cout << "Client got " << result << " from server" << std::endl;
            }
		}
        else
        {
            if (gIsServer)
                std::cout << "Server: ";
            else
            {
                ++gNumInvocations;
                std::cout << "Client: ";
            }
            std::cout << "Stream was closed this iteration." << std::endl;
            ++failedIterations;
        }

		if (!gIsServer &&
			lastKnownInvocations < gNumInvocations &&
			gNumInvocations < kNumIterations)
		{
			// Update just a couple more times to make sure we send pending return
			// values and such.
			spinStream(pStream);
			spinStream(pStream);

			// Disconnect and reconnect

			std::cout << "Disconnecting...";

			pStream->close();
			delete pStream;
			delete pConnection;
			delete pRemoteProc;

//			spinStream(nullptr, false);

			std::cout << "reconnecting...";

			pConnection = new SocketConnection("localhost");
			pStream = new Stream(*pConnection);
			pRemoteProc = new RemoteProcedure< int () >(*pStream, "Proc");
			pStream->registerProcedure(localProc);

			if (pConnection->isOpen())
			{
                std::cout << "connected!" << std::endl;
	            numReconnections++;
			}
			else
			{
                std::cout << "connection failed!" << std::endl;
			}
		}

		// Be nice to the CPU
		boost::xtime time;
		boost::xtime_get(&time, boost::TIME_UTC_);
        // Ten milliseconds
//		time.sec += 1;
		time.nsec += 10000000;
		boost::thread::sleep(time);
	}

	// Update just a couple more times to make sure we send pending return
	// values and such.
	spinStream(pStream);

	if (!gIsServer)
    {
        std::cout << "Successful reconnections: " << numReconnections << std::endl;
    }

	delete pStream;
	delete pConnection;
	delete pRemoteProc;

	if (gIsServer)
    {
		return 0;
    }
	else
    {
		return (numReconnections > 1) ? 0 : 1;
    }
}
