BOOST_HOME ?= /usr

HEADERS = include/MetaRPC/ArgSupport.h    include/MetaRPC/Connection.h       \
          include/MetaRPC/DataChunk.h     include/MetaRPC/EncodingTypes.h    \
          include/MetaRPC/EndianSwap.h    include/MetaRPC/Exceptions.h       \
          include/MetaRPC/MemConnection.h include/MetaRPC/Procedure.h        \
          include/MetaRPC/ReturnSupport.h include/MetaRPC/SocketConnection.h \
          include/MetaRPC/Stream.h

BUILD_DIR = build
PREFIX ?= dist

LIB_SRC = src/MemConnection.cpp src/SocketConnection.cpp src/Stream.cpp
CONNTEST_SRC = examples/conn_test.cpp
LOCALTEST_SRC = examples/localtest.cpp

LIB_OBJ = $(patsubst src/%.cpp,$(BUILD_DIR)/lib/%.o,$(LIB_SRC))
CONNTEST_OBJ = $(patsubst examples/%.cpp,$(BUILD_DIR)/examples/%.o,$(CONNTEST_SRC))
LOCALTEST_OBJ = $(patsubst examples/%.cpp,$(BUILD_DIR)/examples/%.o,$(LOCALTEST_SRC))

ST_LIB = $(BUILD_DIR)/lib/libMetaRPC.a

CONNTEST_EXE = $(BUILD_DIR)/examples/conn_test
LOCALTEST_EXE = $(BUILD_DIR)/examples/localtest


AR ?= ar
CXX ?= g++
CXXFLAGS ?= -g3 -O3 -Wall
CXXFLAGS_FINAL = -std=c++11 -fPIC -DMETARPC_FORCE_SAME_ENDIANESS=1 $(CXXFLAGS)
INCLUDES = -I. -Iinclude -I$(BOOST_HOME)/include

LIBPATH = -L$(BOOST_HOME)/lib -L$(BUILD_DIR)/lib

LIBS = -lMetaRPC -lboost_signals -lboost_thread -lboost_system -lpthread


.PHONY: all build_setup clean distclean


all: build_setup $(ST_LIB) $(CONNTEST_EXE) $(LOCALTEST_EXE)

build_setup:
	mkdir -p $(BUILD_DIR)/lib
	mkdir -p $(BUILD_DIR)/examples


$(BUILD_DIR)/lib/%.o: src/%.cpp $(HEADERS)
	$(CXX) -o $@ -c $< $(INCLUDES) $(CXXFLAGS_FINAL)

$(ST_LIB): $(LIB_OBJ)
	$(AR) rcs $@ $^


$(BUILD_DIR)/examples/%.o: examples/%.cpp $(HEADERS)
	$(CXX) -o $@ -c $< $(INCLUDES) $(CXXFLAGS_FINAL)

$(CONNTEST_EXE): $(CONNTEST_OBJ) $(ST_LIB)
	$(CXX) -o $@ $^ $(CXXFLAGS_FINAL) $(LIBPATH) $(LIBS)

$(LOCALTEST_EXE): $(LOCALTEST_OBJ) $(ST_LIB)
	$(CXX) -o $@ $^ $(CXXFLAGS_FINAL) $(LIBPATH) $(LIBS)


install: all
	mkdir -p $(PREFIX)/bin
	mkdir -p $(PREFIX)/lib
	mkdir -p $(PREFIX)/include/MetaRPC
	cp $(ST_LIB) $(PREFIX)/lib/
	cp $(CONNTEST_EXE) $(PREFIX)/bin/
	cp $(LOCALTEST_EXE) $(PREFIX)/bin/
	cp -r $(HEADERS) $(PREFIX)/include/MetaRPC

clean:
	rm -rf $(BUILD_DIR)

distclean: clean
	rm -rf $(PREFIX)/include/MetaRPC
	rm -f $(PREFIX)/bin/conn_test
	rm -f $(PREFIX)/bin/localtest
	rm -f $(PREFIX)/lib/libMetaRPC.a

